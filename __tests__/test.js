process.env.NODE_ENV = 'test';
const fs = require('fs');
const mongoose = require('mongoose');
const { Category } = require('../models/category');
const { User } = require('../models/user');
const { File } = require('../models/file');
const { Photo } = require('../models/photo');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const expect = chai.expect;

chai.use(chaiHttp);

// eslint-disable-next-line max-statements
describe('Testing API', () => {
    before(done => {
        Category.remove({}).exec();
        User.remove({}).exec();
        File.remove({}).exec();
        Photo.remove({}).exec();
        done();
    });
    let categoryId;
    let userId;
    let photoId;

    describe('/POST categories', () => {
        it('it should POST a category ', done => {
            chai.request(server)
                .post('/api/categories')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({ name: 'city' })
                .end((err, res) => {
                    categoryId = res.body._id;
                    console.log('QQQQQQQQQQ',categoryId);
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('name');
                    done();
                });
        });
    });
    describe('/GET categories', () => {
        it('it should GET all the categories', done => {
            chai.request(server)
                .get('/api/categories')
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('array');
                    expect(res.body).to.have.lengthOf(1);
                    expect(res.body[0].name).to.be.equal('city');
                    done();
                });
        });
    });
    describe('/POST categories with empty name', () => {
        it('it not shouldn POST a category with empty name', done => {
            chai.request(server)
                .post('/api/categories')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({ name: '' })
                .end((err, res) => {
                    expect(res).to.have.status(500);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('error');
                    expect(res.body.error).to.be.equal('FAILED_TO_CREATE_CATEGORY');
                    done();
                });
        });
    });

    describe('/POST users (sign up)', () => {
        it('it should POST a new user', done => {
            chai.request(server)
                .post('/api/signup')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    name: 'ekaterina',
                    email: 'bbbb@mail.com',
                    password: '123' })
                .end((err, res) => {
                    userId = res.body._id;
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('name');
                    expect(res.body).to.have.property('email');
                    expect(res.body).to.have.property('password');
                    done();
                });
        });
    });
    describe('/POST users (sign up) with existing email', () => {
        it('it shouldnt POST a new user with existing email ', done => {
            chai.request(server)
                .post('/api/signup')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    name: 'ekaterina',
                    email: 'bbbb@mail.com',
                    password: '123' })
                .end((err, res) => {
                    expect(res).to.have.status(500);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('error');
                    expect(res.body.error).to.be.equal('EXISTING_EMAIL');
                    done();
                });
        });
    });

    describe('/POST users (sign up) with empty email', () => {
        it('it shouldnt POST a new user with empty email ', done => {
            chai.request(server)
                .post('/api/signup')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    name: 'ekaterina',
                    email: '',
                    password: '123' })
                .end((err, res) => {
                    expect(res).to.have.status(500);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('error');
                    expect(res.body.error).to.be.equal('FAILED_TO_CREATE_USER');
                    done();
                });
        });
    });
    describe('/POST photo', () => {
        it('it should POST a new photo', done => {
            chai.request(server)
                .post('/api/photos')
                .field('name', 'city')
                .field('category_id', categoryId)
                .field('user_id', userId)
                .field('tags', 'blue,red')
                .attach('photo', fs.readFileSync('./resources/city.jpg'), 'city.jpg')
                .end((err, res) => {
                    photoId = res.body._id;
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('status');
                    expect(res.body).to.have.property('message');
                    //expect(res.body).to.have.property('user_id');
                    //expect(res.body).to.have.property('file_id');
                    //expect(res.body).to.have.property('tags');
                    done();
                });
        });
    });
    describe('/POST photo without category', () => {
        it('it shouldnt POST a new photo', done => {
            chai.request(server)
                .post('/api/photos')
                .field('name', 'cat')
                .field('categoty_id', '')
                .field('user_id', userId)
                .field('tags', 'blue,red')
                .attach('photo', fs.readFileSync('./resources/city.jpg'), 'city.jpg')
                .end((err, res) => {
                    expect(res).to.have.status(500);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('error');
                    expect(res.body.error).to.be.equal('FAILED_TO_CREATE_PHOTO');
                    done();
                });
        });
    });
    describe('/GET photos', () => {
        it('it should GET photos', done => {
            chai.request(server)
                .get('/api/photos')
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('array');
                    expect(res.body).to.have.lengthOf(1);
                    done();
                });
        });
    });
    describe('/GET photos by category', () => {
        it('it should GET photos with category = city', done => {
            chai.request(server)
                .get('/api/photos?category=city')
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('array');
                    expect(res.body).to.have.lengthOf(1);
                    done();
                });
        });
    });
    describe('/GET photos by nonexistent category', () => {
        it('it shouldnt GET photos with nonexistent category', done => {
            chai.request(server)
                .get('/api/photos?category=people')
                .end((err, res) => {
                    expect(res).to.have.status(500);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('error');
                    expect(res.body.error).to.be.equal('CATEGORY_NOT_FOUND');
                    done();
                });
        });
    });
    describe('/GET photos by user name', () => {
        it('it should GET photos by user name', done => {
            chai.request(server)
                .get('/api/photos?user=ekaterina')
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('array');
                    expect(res.body).to.have.lengthOf(1);
                    done();
                });
        });
    });
    describe('/GET photos by nonexistent user name', () => {
        it('it shouldnt GET photos with nonexistent user name', done => {
            chai.request(server)
                .get('/api/photos?user=ivan')
                .end((err, res) => {
                    expect(res).to.have.status(500);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('error');
                    expect(res.body.error).to.be.equal('USER_NOT_FOUND');
                    done();
                });
        });
    });
    describe('/GET photos by tags', () => {
        it('it should GET photos by tags', done => {
            chai.request(server)
                .get('/api/photos?tags=blue')
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('array');
                    expect(res.body).to.have.lengthOf(1);
                    done();
                });
        });
    });
    describe('/GET photo by id', () => {
        it('it should GET photo by id', done => {
            chai.request(server)
                .get('/api/photos/' + photoId)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('name');
                    expect(res.body).to.have.property('category_id');
                    expect(res.body).to.have.property('user_id');
                    expect(res.body).to.have.property('file_id');
                    done();
                });
        });
    });
    describe('/GET photo by nonexistent id', () => {
        it('it shouldnt GET photo by nonexistent id', done => {
            chai.request(server)
                .get('/api/photos/454348')
                .end((err, res) => {
                    expect(res).to.have.status(500);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('error');
                    expect(res.body.error).to.be.equal('PHOTO_NOT_FOUND');
                    done();
                });
        });
    });
    describe('/PUT photo by id', () => {
        it('it should update photo by id', done => {
            chai.request(server)
                .put('/api/photos/' + photoId)
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    name: 'lion'
                })
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('category_id');
                    expect(res.body).to.have.property('user_id');
                    expect(res.body).to.have.property('file_id');
                    expect(res.body).to.have.property('name');
                    expect(res.body.name).to.be.equal('lion');
                    done();
                });
        });
    });
    describe('/PUT photo by nonexistent id', () => {
        it('it should update photo by nonexistent id', done => {
            chai.request(server)
                .put('/api/photos/4156169')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    name: 'lion'
                })
                .end((err, res) => {
                    expect(res).to.have.status(500);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('error');
                    expect(res.body.error).to.be.equal('PHOTO_NOT_FOUND');
                    done();
                });
        });
    });
    describe('/DELETE photo by id', () => {
        it('it should delete photo by id', done => {
            chai.request(server)
                .delete('/api/photos/' + photoId)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('category_id');
                    expect(res.body).to.have.property('user_id');
                    expect(res.body).to.have.property('file_id');
                    expect(res.body).to.have.property('name');
                    done();
                });
        });
    });
    describe('/DELETE photo by nonexistent id', () => {
        it('it should delete photo by nonexistent id', done => {
            chai.request(server)
                .delete('/api/photos/4548543')
                .end((err, res) => {
                    expect(res).to.have.status(500);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('error');
                    expect(res.body.error).to.be.equal('PHOTO_NOT_FOUND');
                    done();
                });
        });
    });
    describe('/GET category by id', () => {
        it('it should GET category by id', done => {
            chai.request(server)
                .get('/api/categories/' + categoryId)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('name');
                    done();
                });
        });
    });
    describe('/PUT category by id', () => {
        it('it should update category by id', done => {
            chai.request(server)
                .put('/api/categories/' + categoryId)
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    name: 'nature'
                })
                .end((err, res) => {
                    console.log('!!!!!!!!', categoryId);
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('name');
                    expect(res.body.name).to.be.equal('nature');
                    done();
                });
        });
    });
    describe('/DELETE category by id', () => {
        it('it should delete category by id', done => {
            chai.request(server)
                .delete('/api/categories/' + categoryId)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('name');
                    done();
                });
        });
    });
    describe('/POST users (sign in)', () => {
        it('it should POST a existing user ', done => {
            chai.request(server)
                .post('/api/signin')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    email: 'bbbb@mail.com',
                    password: '123' })
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('name');
                    expect(res.body).to.have.property('email');
                    expect(res.body).to.have.property('password');
                    done();
                });
        });
    });

    describe('/POST users (sign in with wrong password)', () => {
        it('it shouldnt POST a user with wrong password ', done => {
            chai.request(server)
                .post('/api/signin')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    email: 'bbbb@mail.com',
                    password: 'qwerty11' })
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('error');
                    expect(res.body.error).to.be.equal('WRONG_PASSWORD');
                    done();
                });
        });
    });
    describe('/POST users (sign in with nonexistent email)', () => {
        it('it shouldnt POST a user with nonexistent email', done => {
            chai.request(server)
                .post('/api/signin')
                .set('content-type', 'application/x-www-form-urlencoded')
                .send({
                    email: 'kkkkk@mail.com',
                    password: 'qwerty' })
                .end((err, res) => {
                    expect(res).to.have.status(500);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('error');
                    expect(res.body.error).to.be.equal('USER_NOT_FOUND');
                    done();
                });
        });
    });
    describe('/GET user by id', () => {
        it('it should GET user by id', done => {
            chai.request(server)
                .get('/api/users/' + userId)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('name');
                    expect(res.body).to.have.property('email');
                    expect(res.body).to.have.property('password');
                    done();
                });
        });
    });
});
