const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const photoSchema = new Schema(
    {
        file_id: { type: Schema.ObjectId, ref: 'Image', required: true },
        category_id: { type: Schema.ObjectId, ref: 'Category', required: true },
        user_id: { type: Schema.ObjectId, ref: 'User', required: true },
        date: { type: Date },
        name: { type: String, max: 100 },
        description: { type: String, max: 500 },
        tags: { type: Array },
        isBanned: { type: Boolean }
    }
);
const Photo = mongoose.model('Photo', photoSchema);

module.exports = { Photo };
