const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const categorySchema = new Schema(
    {
        name: { type: String, required: true, max: 100 }
    }
);
const Category = mongoose.model('Category', categorySchema);

module.exports = { Category };
