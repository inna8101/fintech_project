const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const userSchema = new Schema(
    {
        name: { type: String, required: true, max: 100 },
        email: { type: String, required: true, max: 50 },
        password: { type: String, required: true, max: 50 },
        token: {type: String },
        isAdmin: { type: Boolean }
    }
);

const User = mongoose.model('User', userSchema);

module.exports = { User };
