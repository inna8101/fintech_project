const {
    getRandomPhoto,
    getFileById,
    getPhotoById,
    getPhotos,
    postPhoto,
    deletePhoto,
    updatePhoto,
    getCategories,
    getCategoryById,
    postCategory,
    deleteCategory,
    signUp,
    signIn,
    getUserById,
    imprint
} = require('./functions');

module.exports = app => {
    app.get('/api/randomfile', getRandomPhoto);

    app.post('/api/photos', postPhoto);

    app.get('/api/files/:id', getFileById);

    app.get('/api/photos/:id', getPhotoById);

    app.get('/api/photos', getPhotos);

    app.put('/api/photos/:id', updatePhoto);

    app.delete('/api/photos/:id', deletePhoto);

    app.post('/api/categories', postCategory);

    app.delete('/api/categories/:name', deleteCategory);

    app.get('/api/categories', getCategories);

    app.get('/api/categories/:id', getCategoryById); // нужны тесты

    app.post('/api/signup', signUp);

    app.post('/api/signin', signIn);

    app.get('/api/users/:id', getUserById); // нужны тесты

    app.post('/api/imprint', imprint);
};
