module.exports = {
    CATEGORY_NOT_FOUND: {
        error: 'CATEGORY_NOT_FOUND',
        text: 'Category doesn\'t exist'
    },
    EXISTING_CATEGORY: {
        error: 'EXISTING_CATEGORY',
        text: 'This category already exist'
    },
    FILE_NOT_FOUND: {
        error: 'FILE',
        text: 'FILE doesn\'t exist'
    },
    PHOTO_NOT_FOUND: {
        error: 'PHOTO_NOT_FOUND',
        text: 'Photo doesn\'t exist'
    },
    USER_NOT_FOUND: {
        error: 'USER_NOT_FOUND',
        text: 'User with such email doesn\'t exist'
    },
    PHOTOS_WITH_TAGS_NOT_FOUND: {
        error: 'PHOTOS_WITH_TAGS_NOT_FOUND',
        text: 'No photos found with such tags'
    },
    FAILED_TO_CREATE_CATEGORY: {
        error: 'FAILED_TO_CREATE_CATEGORY',
        text: 'failed to create category'
    },
    FAILED_TO_CREATE_USER: {
        error: 'FAILED_TO_CREATE_USER',
        text: 'failed to create user'
    },
    FAILED_TO_CREATE_FILE: {
        error: 'FAILED_TO_CREATE_FILE',
        text: 'failed to create file'
    },
    FAILED_TO_CREATE_PHOTO: {
        error: 'FAILED_TO_CREATE_PHOTO',
        text: 'failed to create photo'
    },
    EXISTING_EMAIL: {
        error: 'EXISTING_EMAIL',
        text: 'User with such email already exist'
    },
    WRONG_PASSWORD: {
        error: 'WRONG_PASSWORD',
        text: 'wrong password'
    },
    ACCESS_DENIED: {
        error: 'ACCESS_DENIED',
        text: 'Access denied!'
    }
};
