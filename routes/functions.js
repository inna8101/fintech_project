const { Category } = require('../models/category');
const { User } = require('../models/user');
const { File } = require('../models/file');
const { Photo } = require('../models/photo');
const fs = require('fs');
const nodeMailer = require('nodemailer');
const config = require('config');
const jwt = require('jsonwebtoken');

const errors = require('./errors');

async function getRandomPhoto(req, res) { 
    try {
        const [randomPhoto] = await File.aggregate([{ $sample: { size: 1 } }]).exec();

        res.send(randomPhoto._id);
    } catch (error) {
        return res.status(404).send(errors.PHOTO_NOT_FOUND);
    }
}


async function getFileById(req, res) {
    try {
        const file = await File.findById(req.params.id).exec();

        res.contentType('image/jpeg');
        res.write(file.fileBuf);
        res.end();
    } catch (error) {
        console.log(error);
        return res.status(404).send(errors.FILE_NOT_FOUND);
    }
}

async function getPhotoById(req, res) {
    try {
        const photo = await Photo.findById(req.params.id).exec();

        res.send(photo);
    } catch (error) {
        console.log(error);
        return res.status(404).send(errors.PHOTO_NOT_FOUND);
    }
}

async function getPhotosByTags(tags, res) {
    const tagsN = tags.split(/[ :;?!~,'"&|()<>{}\[\]\r\n/\\]+/);

    try {
        const photosWithTags = await Photo.find({ tags: { $in: tagsN } }).exec();

        return res.send(photosWithTags);
    } catch (error) {
        console.log(error);
        return res.status(404).send(errors.PHOTOS_WITH_TAGS_NOT_FOUND);
    }
}

async function getPhotosByCategory(needCategory, res) {
    const category = await Category.findOne({ name: needCategory }).exec();

    if (!category) {
        return res.status(404).send(errors.CATEGORY_NOT_FOUND);
    }
    try {
        const photosByCategory = await Photo.find({ category_id: category._id }).exec();

        return res.send(photosByCategory);
    } catch (error) {
        console.log(error);
        return res.status(404).send(errors.PHOTOS_NOT_FOUND);
    }
}

async function getPhotosByUser(userId, res) {
    try {
        const photosByUser = await Photo.find({ user_id: userId }).exec();

        return res.send(photosByUser);
    } catch (error) {
        console.log(error);
        return res.status(404).send(errors.PHOTOS_NOT_FOUND);
    }
}
async function getPhotos(req, res) {
    const needTags = req.query.tags;

    if (needTags) {
        getPhotosByTags(needTags, res);
        return;
    }
    const needCategory = req.query.category;

    if (needCategory) {
        getPhotosByCategory(needCategory, res);
        return;
    }
    const needUserId = req.query.userId;

    if (needUserId) {
        getPhotosByUser(needUserId, res);
        //return;
    }
  
}

function postPhoto(req, res) {
    if (!req.headers['x-access-token']) {
        return res.status(401).send(errors.ACCESS_DENIED);
    }
    const newPhoto = { isBanned: false };
    let today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();

    today = mm + '.' + dd + '.' + yyyy;
    File.create({ fileBuf: fs.readFileSync(req.file.path) }, (err, file) => {
        if (err) {
            console.log(err);
            return res.status(500).send(errors.FAILED_TO_CREATE_FILE);
        }
        idFile = file._id;
        Object.assign(newPhoto, req.body, { file_id: file._id, date: today });
        console.log('ttttt', req.body.tags);
        if (req.body.tags) {
            newPhoto.tags = req.body.tags.split(/[ :;?!~,'"&|()<>{}\[\]\r\n/\\]+/);
        }
        Photo.create(newPhoto, (error, photo) => {
            if (error) {
                console.log(error);
                return res.status(500).send(errors.FAILED_TO_CREATE_PHOTO);
            }
            return res.send({
                status: 'OK',
                message: 'Your photo uploaded!'
            });
        });
    });
}
async function deletePhoto(req, res) {
    if (!req.headers['x-access-token']) {
        return res.status(401).send(errors.ACCESS_DENIED);
    }
    try {
        const deletedPhoto = await Photo.findByIdAndDelete(req.params.id).exec();
        const deletedFile = await File.findByIdAndDelete(deletedPhoto.file_id).exec();

        res.send({
            status: 'OK',
            message: 'This photo deleted!'
        });
    } catch (error) {
        console.log(error);
        return res.status(404).send(errors.PHOTO_NOT_FOUND);
    }
}

async function updatePhoto(req, res) {
    try {
        const updatedPhoto = await Photo.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true }).exec();

        res.send({
            status: 'OK',
            message: 'Your photo updated!'
        });
    } catch (error) {
        console.log(error);
        return res.status(404).send(errors.PHOTO_NOT_FOUND);
    }
}

async function getCategories(req, res) {
    try {
        const categories = await Category.find({}).exec();

        res.send(categories); // нужны только названия
    } catch (error) {
        console.log(error);
        return res.status(404).send(errors.CATEGORY_NOT_FOUND);
    }
}
async function getCategoryById(req, res) {
    try {
        const category = await Category.findById(req.params.id).exec();

        res.send(category);
    } catch (error) {
        console.log(error);
        return res.status(404).send(errors.CATEGORY_NOT_FOUND);
    }
}

async function postCategory(req, res) {
    if (!req.headers['x-access-token']) {
        return res.status(401).send(errors.ACCESS_DENIED);
    }
    const sameCategory = await Category.findOne({ name: req.body.name }).exec();

    if (sameCategory) {
        return res.status(408).send(errors.EXISTING_CATEGORY);
    }
    Category.create({ name: req.body.name }, (err, category) => {
        if (err) {
            console.log(err);
            return res.status(500).send(errors.FAILED_TO_CREATE_CATEGORY);
        }
        res.send(category);
    });
}
async function deleteCategory(req, res) {
    if (!req.headers['x-access-token']) {
        return res.status(401).send(errors.ACCESS_DENIED);
    }
    try {
        const deletedCategory = await Category.findOneAndDelete({ name: req.params.name }).exec();

        res.send(deletedCategory);
    } catch (error) {
        console.log(error);
        res.status(404).send(errors.CATEGORY_NOT_FOUND);
    }
}
async function signUp(req, res) {
    const newUser = {};

    Object.assign(newUser, req.body);
    if (!req.body.isAdmin) {
        newUser.isAdmin = false;
    }
    const userWithSameEmail = await User.findOne({ email: newUser.email }).exec();

    if (!userWithSameEmail) {
        User.create(newUser, (err, user) => {
            if (err) {
                console.log(err);
                return res.status(500).send(errors.FAILED_TO_CREATE_USER);
            }
            const token = jwt.sign({ id: user._id }, config.secret);
            
            user.token = token;
            res.send(user);
        });
    } else {
        return res.status(409).send(errors.EXISTING_EMAIL);
    }
}

async function signIn(req, res) {
    try {
        const user = await User.findOne({ email: req.body.email }).exec();
        
        if (user.password === req.body.password) {
            const token = jwt.sign({ id: user._id }, config.secret);
            
            user.token = token;
            res.status(200).send(user);
        } else {
            return res.status(401).send(errors.WRONG_PASSWORD);
        }
    } catch (error) {
        console.log(error);
        return res.status(404).send(errors.USER_NOT_FOUND);
    }
}
async function getUserById(req, res) {
    try {
        const user = await User.findById(req.params.id).exec();

        res.send(user);
    } catch (error) {
        console.log(error);
        return res.status(404).send(errors.USER_NOT_FOUND);
    }
}
function imprint(req, res) {
    const transporter = nodeMailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'inna8101@gmail.com',
            pass: 'vladbylka3101'
        }
    });

    const mailOptions = {
        from: '"fintech" <xx@gmail.com>',
        to: req.body.recipient.email,
        subject: 'Imprint photo',
        text: `${req.body.name} (${req.body.email}) requested an imprint of your photo called ${req.body.photo.name} `
    };

    transporter.sendMail(mailOptions, error => {
        if (error) {
            return console.log(error);
        }

        res.send({
            status: 'OK',
            message: 'Imprint requested!'
        });
    });
}

module.exports = {
    getRandomPhoto,
    getFileById,
    getPhotoById,
    getPhotos,
    postPhoto,
    deletePhoto,
    updatePhoto,
    getCategories,
    getCategoryById,
    postCategory,
    deleteCategory,
    signUp,
    signIn,
    getUserById,
    imprint
};
